import { MigrationInterface, QueryRunner } from "typeorm";

export class USERRelationsToPlayer1641148043042 implements MigrationInterface {
    name = "USERRelationsToPlayer1641148043042";

    public async up(queryRunner: QueryRunner): Promise<void> {
        // see one to one relation example: https://www.cybertec-postgresql.com/en/1-to-1-relationship-in-postgresql-for-real/
        await queryRunner.query(`
            ALTER TABLE "user" 
            ADD CONSTRAINT "FK_08df5d6d77f6191e53715afa910" FOREIGN KEY ("id") REFERENCES "player"("id") 
            DEFERRABLE INITIALLY DEFERRED
        `);
        await queryRunner.query(`
            ALTER TABLE "player" 
            ADD CONSTRAINT "FK_08df5d6d77f6191e53715afa911" FOREIGN KEY ("id") REFERENCES "user"("id") 
            DEFERRABLE INITIALLY DEFERRED
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_08df5d6d77f6191e53715afa910"`);
        await queryRunner.query(`ALTER TABLE "player" DROP CONSTRAINT "FK_08df5d6d77f6191e53715afa911"`);
    }

}
