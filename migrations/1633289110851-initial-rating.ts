import {MigrationInterface, QueryRunner} from "typeorm";

export class initialRating1633289110851 implements MigrationInterface {
    name = 'initialRating1633289110851'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "rating" ("id" SERIAL NOT NULL, "value" integer NOT NULL, "playerId" integer, CONSTRAINT "PK_ecda8ad32645327e4765b43649e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "rating" ADD CONSTRAINT "FK_7b7b67ede0cccc5f041b2f976a2" FOREIGN KEY ("playerId") REFERENCES "player"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "rating" DROP CONSTRAINT "FK_7b7b67ede0cccc5f041b2f976a2"`);
        await queryRunner.query(`DROP TABLE "rating"`);
    }

}
