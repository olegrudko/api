import { Injectable, Logger, NotFoundException, BadRequestException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Player } from "./player.entity";
import { IPlayer } from "@shared/shared/player/player.interface";

@Injectable()
export class PlayerService {
    private readonly logger = new Logger(PlayerService.name);

    constructor(@InjectRepository(Player) private playerRepository: Repository<Player>) {}

    async findOne(id: number): Promise<Player> {
        const player = await this.playerRepository.findOne({
            where: { id },
        });
        if (!player) {
            this.logger.warn(`Player with id: ${id} was not found`);
        }
        return player;
    }

    async findOneWithRelations(id: number, relations: string[]): Promise<any> {
        const player = await this.playerRepository.findOne({
            where: { id },
            relations,
        });
        if (!player) {
            this.logger.warn(`Player with id: ${id} was not found`);
        }
        this.logger.log(player);
        return player;
    }

    async create(playerDTO: IPlayer): Promise<Player> {
        const createdPlayer = await this.playerRepository.save({
            nickname: playerDTO.nickname,
            createdAt: new Date(),
            isActive: true,
        });
        this.logger.log(`User created with id ${createdPlayer.id}`);
        return createdPlayer;
    }

    async update(id: number, payload: Partial<Omit<IPlayer, "id">>) {
        let playerToUpdate = await this.findOne(id);
        if (!playerToUpdate) {
            throw new NotFoundException("Player was not found");
        }
        playerToUpdate = { ...playerToUpdate, ...payload };
        return this.playerRepository.save(playerToUpdate);
    }
}
