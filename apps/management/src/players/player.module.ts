import { Module } from "@nestjs/common";
import { PlayerService } from "./player.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { config } from "../config";
import { Player } from "./player.entity";

@Module({
    providers: [PlayerService],
    imports: [
        TypeOrmModule.forRoot(config.database),
        TypeOrmModule.forFeature([Player]),
    ],
})
export class PlayerModule {}
