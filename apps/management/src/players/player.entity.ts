import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from "typeorm";
import { IPlayer } from "@shared/shared/player/player.interface";
import { User } from "../users/user.entity";

@Entity()
export class Player implements IPlayer {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => User) //OneToOne before will be allowed multiple games to one user
    user: User;

    @Column()
    nickname: string;
}
