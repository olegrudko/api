import { Controller, Patch, Request, Body, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import { UserService } from "./user.service";
import { ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";
import { User } from "./user.entity";

@ApiTags("User")
@Controller("users")
export class UserController {
    constructor(private usersService: UserService) {}

    @ApiResponse({ status: 200, description: "Updated user", type: User })
    @ApiParam({ name: "name" })
    @UseGuards(JwtAuthGuard)
    @Patch(":name")
    changeName(@Request() req, @Body() body) {
        return this.usersService.changeName(req.params.name, body.username);
    }
}
