import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from "typeorm";
import { IUser } from "@shared/shared/user/user.interface";
import { Player } from "../players/player.entity";

@Entity()
export class User implements IUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    username: string;

    @Column()
    password: string;

    @Column()
    createdAt: Date;

    @Column()
    isActive: boolean;

    @OneToOne(() => Player) //OneToOne before will be allowed multiple games to one user
    @JoinColumn({ name: "id" })
    player: Player;
}
