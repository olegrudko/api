import { Injectable, Logger, NotFoundException, BadRequestException } from "@nestjs/common";
import { User } from "./user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { RegistrationRequest } from "@shared/shared/auth/registration.request";
import { DBTableName } from "@shared/shared/constants/db-table-name.enum";

enum RepositoryActionErrors {
    CHECK_FOR_UNIQUE = "23505",
}

@Injectable()
export class UserService {
    private readonly logger = new Logger(UserService.name);

    constructor(@InjectRepository(User) private usersRepository: Repository<User>) {}

    async findOne(username: string, validate = false, nestedPlayer = false): Promise<User> {
        const relations: string[] = nestedPlayer ? [DBTableName.player] : [];
        const user = await this.usersRepository.findOne({
            where: { username },
            relations,
        });
        if (validate && !user) {
            throw new NotFoundException("user was not found");
        }
        return user;
    }

    async create(registration: RegistrationRequest): Promise<User> {
        const user = await this.findOne(registration.username);
        if (user) {
            return user; // TODO: dangerous behaviour
        }

        const userCreated = await this.usersRepository.save({
            username: registration.username,
            password: registration.password,
            createdAt: new Date(),
            isActive: true,
        });
        this.logger.log(`user created ${userCreated.id}`);
        return userCreated;
    }

    async changeName(username: string, name: string): Promise<User> {
        const user = await this.findOne(username, true);
        try {
            await this.usersRepository.update(user.id, { username: name });
            return this.usersRepository.findOne(user.id);
        } catch (error) {
            if (error.code === RepositoryActionErrors.CHECK_FOR_UNIQUE) {
                throw new BadRequestException(`the '${name}' name is already used`);
            }
        }
    }
}
