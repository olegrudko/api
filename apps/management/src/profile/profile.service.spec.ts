import { Test, TestingModule } from "@nestjs/testing";
import { ProfileService } from "./profile.service";
import { getConnection, getRepository } from "typeorm";
import { config } from "../config";
import { Rating } from "../ratings/rating.entity";
import { User } from "../users/user.entity";
import { Player } from "../players/player.entity";
import { UserModule } from "../users/user.module";
import { RatingModule } from "../ratings/rating.module";
import { PlayerModule } from "../players/player.module";
import { RatingService } from "../ratings/rating.service";
import { PlayerService } from "../players/player.service";
import { TypeOrmModule } from "@nestjs/typeorm";

describe("ProfileService", () => {
    let profileService: ProfileService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ProfileService, RatingService, PlayerService], // Provide all services that used in test
            imports: [
                UserModule,
                RatingModule,
                PlayerModule,
                TypeOrmModule.forRoot(config.database), // Add typeorm module. It will create connection and make possible use typeorm in tests
                TypeOrmModule.forFeature([Player, Rating]), // Define models that will be used in tests
            ],
        }).compile();

        profileService = module.get<ProfileService>(ProfileService);
    });

    afterEach(async () => {
        const connection = getConnection();
        await connection.dropDatabase();
        return connection.close();
    });

    it("find profile by player id", async () => {
        const userRepository = getRepository(User);
        const playerRepository = getRepository(Player);
        const ratingRepository = getRepository(Rating);

        const user = await userRepository.save({
            // Use save method cause it returns id of saved object
            username: "test",
            password: "test",
            createdAt: new Date(),
            isActive: false,
        });
        const player = await playerRepository.save({
            nickname: "cat",
            userId: user.id,
        });
        await ratingRepository.save({
            playerId: player.id,
            value: 123,
        });

        const profile = await profileService.findOne(player.id);
        expect(profile.nickname).toMatch("cat");
        expect(profile.rating).toBe(123);
    });
});
