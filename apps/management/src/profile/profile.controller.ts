import { Controller, Get, Patch, Request, Body, UseGuards } from "@nestjs/common";
import { ProfileService } from "./profile.service";
import { ApiBody, ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";
import { ProfileResponse } from "@shared/shared/profile/profile.response";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";

@ApiTags("Profile")
@Controller("profile")
export class ProfileController {
    constructor(private readonly profileService: ProfileService) {}

    @ApiResponse({ status: 200, description: "Get profile", type: ProfileResponse })
    @ApiParam({ name: "playerId" })
    @Get(":playerId")
    getProfile(@Request() req): Promise<ProfileResponse> {
        console.log("Get profile: " + req);
        return this.profileService.findOne(req.params.playerId);
    }

    @ApiParam({ name: "playerId" })
    @ApiBody({ schema: {
        type: "object",
        properties: {
            nickname: { type: "string", example: "player1" }
        }
    } })
    @ApiResponse({ status: 200, schema: { example: "nickname" }, description: "Updated player's nickname" })
    @UseGuards(JwtAuthGuard)
    @Patch(":playerId/nickname")
    changePlayerNickname(@Request() req, @Body() body) {
        console.log("nickname", body.nickname);
        return this.profileService.changePlayerNickname(req.params.playerId, body.nickname);
    }
}
