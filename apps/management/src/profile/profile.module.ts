import { Module } from "@nestjs/common";
import { ProfileController } from "./profile.controller";
import { ProfileService } from "./profile.service";
import { UserModule } from "../users/user.module";
import { RatingModule } from "../ratings/rating.module";
import { PlayerModule } from "../players/player.module";
import { RatingService } from "../ratings/rating.service";
import { PlayerService } from "../players/player.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { config } from "../config";
import { Player } from "../players/player.entity";
import { Rating } from "../ratings/rating.entity";

@Module({
    controllers: [ProfileController],
    providers: [ProfileService, RatingService, PlayerService],
    imports: [
        UserModule,
        RatingModule,
        PlayerModule,
        TypeOrmModule.forRoot(config.database),
        TypeOrmModule.forFeature([Player, Rating]),
    ],
})
export class ProfileModule {}
