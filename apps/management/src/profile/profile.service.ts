import { Injectable } from "@nestjs/common";
import { PlayerService } from "../players/player.service";
import { ProfileResponse } from "@shared/shared/profile/profile.response";
import { Rating } from "../ratings/rating.entity";
import { RatingService } from "../ratings/rating.service";

@Injectable()
export class ProfileService {
    constructor(
        private readonly playerService: PlayerService,
        private readonly ratingService: RatingService,
    ) {}

    async findOne(playerId: number): Promise<ProfileResponse> {
        const rating: Rating = await this.ratingService.getRatingWithRelations(playerId);

        return {
            nickname: rating.player.nickname,
            rating: rating.value,
            playerId,
        };
    }

    async changePlayerNickname(playerId: number, nickname: string): Promise<any> {
        let playerUpdated = await this.playerService.update(playerId, { nickname });
        return playerUpdated.nickname;
    }
}
