import { Body, Controller, Post, Request, UseGuards } from "@nestjs/common";
import { LocalAuthGuard } from "./local-auth.guard";
import { AuthService } from "./auth.service";
import { UserService } from "../users/user.service";
import { ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";
import { AuthResponse } from "@shared/shared/auth/auth.response";
import { RegistrationRequest } from "@shared/shared/auth/registration.request";

@ApiTags("Auth")
@Controller("auth")
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UserService,
    ) {}

    @ApiResponse({ status: 200, description: "Login", type: AuthResponse })
    @UseGuards(LocalAuthGuard)
    @Post("login")
    async login(@Request() req): Promise<AuthResponse> {
        return this.authService.login(req.user);
    }

    @ApiResponse({ status: 200, description: "Registration", type: AuthResponse })
    @ApiParam(RegistrationRequest)
    @Post("registration")
    async registration(@Body() body: RegistrationRequest) {
        //Todo: also create record for player and rating data
        const user = await this.usersService.create(body);
        return this.authService.login(user);
    }
}
