import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from "./constants";
import { JwtStrategy } from "./jwt.strategy";
import { UserModule } from "../users/user.module";
import { LocalStrategy } from "./local.strategy";
import { AuthController } from "./auth.controller";
import { TokenService } from './token.service';

@Module({
    imports: [
        UserModule,
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: "60s" },
        }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy, TokenService],
    exports: [AuthService],
    controllers: [AuthController],
})
export class AuthModule {}
