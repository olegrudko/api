import { Injectable, Logger } from "@nestjs/common";
import { User } from "../users/user.entity";
import { JwtService } from "@nestjs/jwt";
import { TokenData } from "@shared/shared";

@Injectable()
export class TokenService {
    private readonly logger = new Logger("token-service");

    constructor(private jwtService: JwtService) {}

    generateToken(user: User) {
        if (!user.player) {
            const error = new Error("Player must be nested in user");
            this.logger.error(error);
            throw error;
        }
        const payload: TokenData = {
            username: user.username,
            userId: user.id,
            playerId: user.player.id,
            nickname: user.player.nickname,
        };

        return this.jwtService.sign(payload);
    }
}
