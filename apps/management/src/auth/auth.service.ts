import { Injectable } from "@nestjs/common";
import { UserService } from "../users/user.service";
import { User } from "../users/user.entity";
import { AuthResponse } from "@shared/shared/auth/auth.response";
import { TokenService } from "./token.service";

@Injectable()
export class AuthService {
    constructor(
        private usersService: UserService,
        private tokenService: TokenService
    ) {
    }

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.usersService
            .findOne(username, false, true);

        if (user && user.password === password) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async login(user: User): Promise<AuthResponse> {
        return {
            access_token: this.tokenService.generateToken(user)
        };
    }
}
