import { TypeOrmModuleOptions } from "@nestjs/typeorm/dist/interfaces/typeorm-options.interface";
import { User } from "./users/user.entity";
import { RatingStrategyType } from "./ratings/strategy/rating.strategy.enum";
import { Rating } from "./ratings/rating.entity";
import { Player } from "./players/player.entity";

export const config: { database: TypeOrmModuleOptions; [key: string]: any } = {
    database: {
        type: "postgres",
        host: process.env.DB_HOST || "localhost",
        port: +process.env.DB_PORT || 5432,
        username: process.env.DB_USERNAME || "postgres",
        password: process.env.DB_PASSWORD || "postgres",
        database: process.env.DB_NAME || "auth",
        entities: [User, Player, Rating],
        synchronize: process.env.DB_SYNCHRONIZE === "true",
    },
    ratingStrategyType: process.env.RATING_STRATEGY || RatingStrategyType.GrowingSubtract,
};
