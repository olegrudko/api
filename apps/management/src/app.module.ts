import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "./auth/auth.module";
import { UserModule } from "./users/user.module";
import { config } from "./config";
import { ProfileModule } from "./profile/profile.module";
import { RatingModule } from "./ratings/rating.module";
import { PlayerModule } from "./players/player.module";

@Module({
    imports: [
        TypeOrmModule.forRoot(config.database),
        UserModule,
        AuthModule,
        ProfileModule,
        RatingModule,
        PlayerModule,
    ],
})
export class AppModule {}
