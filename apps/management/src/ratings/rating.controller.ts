import { Body, Controller, Get, Post, Request } from "@nestjs/common";
import { RatingService } from "./rating.service";
import { ApplyRatingRequest } from "@shared/shared/rating/apply-rating.request";
import { ApplyRatingResponse } from "@shared/shared/rating/apply-rating.response";
import { Rating } from "./rating.entity";
import { ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";
import { RatingResponse } from "@shared/shared/rating/rating.response";

@ApiTags("Rating")
@Controller("/rating")
export class RatingController {
    constructor(private readonly ratingsService: RatingService) {}

    @ApiResponse({
        status: 200,
        description: "Apply and recalculate new rating ",
        type: ApplyRatingResponse,
    })
    @ApiParam(ApplyRatingRequest)
    @Post("/apply")
    async applyNewRating(@Body() body: ApplyRatingRequest): Promise<ApplyRatingResponse> {
        return this.ratingsService.applyRating(body.playerId, body.averageRating, body.position);
    }

    @ApiResponse({ status: 200, description: "Rating value of a player", type: RatingResponse })
    @ApiParam({ name: "playerId", description: "Player id whose rating will be returned" })
    @Get("/:playerId")
    async getRating(@Request() req): Promise<number> {
        const rating: Rating = await this.ratingsService.getRating(req.params["playerId"]);
        return rating.value;
    }
}
