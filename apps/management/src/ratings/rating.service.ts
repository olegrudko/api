import { Injectable, Logger } from "@nestjs/common";
import { Rating } from "./rating.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { RatingStrategy } from "./strategy/rating-strategy.interface";
import { RatingStrategyType } from "./strategy/rating.strategy.enum";
import { GrowingSubtractRatingStrategy } from "./strategy/growing-subtract.rating-strategy";
import { WinData } from "./shared/winData";
import { config } from "../config";
import { ApplyRatingResponse } from "@shared/shared/rating/apply-rating.response";

@Injectable()
export class RatingService {
    private readonly logger = new Logger(RatingService.name);
    private ratingStrategy: RatingStrategy;

    constructor(@InjectRepository(Rating) private ratingRepository: Repository<Rating>) {
        this.ratingStrategy = this.createRatingStrategy();
    }

    public async applyRating(
        playerId: number,
        averageRating: number,
        place: number,
    ): Promise<ApplyRatingResponse> {
        if (!playerId) {
            const error = new Error(`${RatingService.name} applyRating: player id is not defined`);
            this.logger.error(error);
            throw error;
        }
        const playerRating: Rating = await this.getRating(playerId);
        if (!playerRating) {
            // TODO: create errors
            throw new Error("Player rating is not found");
        }

        const winData: WinData = {
            playerId,
            place,
            averageRating,
            currentRating: playerRating.value,
        };
        const newRating: ApplyRatingResponse = this.ratingStrategy.calculateRating(winData);
        await this.ratingRepository.update(playerId, { value: newRating.newRating });
        return newRating;
    }

    public async getRating(playerId): Promise<Rating> {
        return this.ratingRepository.findOne({
            where: {
                playerId,
            },
        });
    }

    public async getRatingWithRelations(playerId): Promise<Rating> {
        return this.ratingRepository.findOne({
            where: {
                playerId,
            },
            relations: ["player"],
        });
    }

    public async createRating(playerId): Promise<Rating> {
        return this.ratingRepository.create({
            playerId: playerId,
            value: 0,
        });
    }

    private createRatingStrategy(): RatingStrategy {
        switch (config.ratingStrategyType) {
            case RatingStrategyType.GrowingSubtract:
                return new GrowingSubtractRatingStrategy();
            default:
                throw new Error("Rating strategy is not defined");
        }
    }
}
