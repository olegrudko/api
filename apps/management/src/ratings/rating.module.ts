import { TypeOrmModule } from "@nestjs/typeorm";
import { config } from "../config";
import { RatingController } from "./rating.controller";
import { RatingService } from "./rating.service";
import { Rating } from "./rating.entity";
import { Module } from "@nestjs/common";

@Module({
    imports: [
        TypeOrmModule.forRoot(config.database),
        TypeOrmModule.forFeature([Rating]),
    ],
    controllers: [RatingController],
    providers: [RatingService],
})
export class RatingModule {}
