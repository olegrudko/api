import { WinData } from "../shared/winData";
import { ApplyRatingResponse } from "@shared/shared/rating/apply-rating.response";

export interface RatingStrategy {
    calculateRating(winData: WinData): ApplyRatingResponse;
}
