import { RatingStrategy } from "./rating-strategy.interface";
import { WinData } from "../shared/winData";
import { ApplyRatingResponse } from "@shared/shared/rating/apply-rating.response";

export class GrowingSubtractRatingStrategy implements RatingStrategy {
    public calculateRating(winData: WinData): ApplyRatingResponse {
        const ratingMultiplier = this.calculateWinCoefficient(winData.place);
        let newRating;
        if (winData.place === 1 || winData.place === 2) {
            newRating = this.calculateWinRating(
                winData,
                ratingMultiplier,
            );
        } else {
            newRating = this.calculateLoseRating(
                winData,
                ratingMultiplier,
            );
        }
        return {
            playerId: winData.playerId,
            oldRating: winData.currentRating,
            newRating: winData.currentRating + newRating,
        };
    }

    private calculateWinRating(
        winData: WinData,
        multiplier: number,
    ): number {
        const currentRating = winData.currentRating || 1;
        const averageRating = winData.averageRating || 1;
        const scale = 2 - 1 / (averageRating/currentRating);
        const newRating = Math.floor(3000 / (currentRating + 150) * 8 * scale);
        return newRating * multiplier;
    }

    private calculateLoseRating(
        winData: WinData,
        multiplier: number,
    ): number {
        const currentRating = winData.currentRating || 1;
        const averageRating = winData.averageRating || 1;
        const scale = currentRating / averageRating;
        let newRating = Math.floor(
            (Math.pow(currentRating, 0.5) - 22.36067) * scale,
        );
        newRating = newRating < 0 ? 0 : newRating;
        return newRating * multiplier;
    }

    private calculateWinCoefficient(place: number): number {
        switch (place) {
            case 1:
                return 1;
            case 2:
                return 0.5;
            case 3:
                return -0.5;
            default:
                return -1;
        }
    }
}
