import { GrowingSubtractRatingStrategy } from "./growing-subtract.rating-strategy";
import { WinData } from "../shared/winData";
import { WinRatingResult } from "../shared/win-rating-result";


describe("RatingsController", () => {
    let ratingStrategy: GrowingSubtractRatingStrategy;

    beforeAll(async () => {
        ratingStrategy = new GrowingSubtractRatingStrategy();
    })

    beforeEach(async () => {

    });

    describe("Change rating on win", () => {
        it('Add rating to player with 0 rating value"', () => {
            const winData: WinData = {
                averageRating: 0,
                place: 1,
                player: {
                    id: "0",
                    rating: 0   // 0 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 0,
                newRating: 158
            });
        });

        it('Add rating to player with 1000 rating value"', () => {
            const winData: WinData = {
                averageRating: 1000,
                place: 1,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 1020
            });
        });

        it('Add rating to player with 2000 rating value"', () => {
            const winData: WinData = {
                averageRating: 2000,
                place: 1,
                player: {
                    id: "0",
                    rating: 2000   // 2000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 2000,
                newRating: 2011
            });
        });

        it('Add half reward rating to player with 1000 rating value and 2-nd place"', () => {
            const winData: WinData = {
                averageRating: 1000,
                place: 2,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 1010
            });
        });

        it('Add more rating to player with 1000 rating value and 1500 overage players rating"', () => {
            const winData: WinData = {
                averageRating: 1500,
                place: 1,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 1027
            });
        });

        it('Add more half rating to player with 1000 rating value and 1500 overage players rating and place 2"', () => {
            const winData: WinData = {
                averageRating: 1500,
                place: 2,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 1013.5
            });
        });
    });

    describe("Change rating on lose", () => {
        it('Dont subtract rating from player with 0 rating value"', () => {
            const winData: WinData = {
                averageRating: 0,
                place: 4,
                player: {
                    id: "0",
                    rating: 0   // 0 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 0,
                newRating: 0
            });
        });

        it('Subtract rating from player with 1000 rating value"', () => {
            const winData: WinData = {
                averageRating: 1000,
                place: 4,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 991
            });
        });

        it('Subtract rating from player with 2000 rating value"', () => {
            const winData: WinData = {
                averageRating: 2000,
                place: 4,
                player: {
                    id: "0",
                    rating: 2000   // 2000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 2000,
                newRating: 1978
            });
        });

        it('Subtract half rating from player with 1000 rating value and 3-nd place"', () => {
            const winData: WinData = {
                averageRating: 1000,
                place: 3,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 995.5
            });
        });

        it('Subtract more rating from player with 1000 rating value and 500 overage players rating"', () => {
            const winData: WinData = {
                averageRating: 500,
                place: 4,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 982
            });
        });

        it('Add more half rating to player with 1000 rating value and 500 overage players rating and place 3"', () => {
            const winData: WinData = {
                averageRating: 500,
                place: 3,
                player: {
                    id: "0",
                    rating: 1000   // 1000 value
                }
            }

            const result: WinRatingResult = ratingStrategy.calculateRating(winData);
            expect(result).toEqual({
                userId: "0",
                oldRating: 1000,
                newRating: 991
            });
        });
    });
});
