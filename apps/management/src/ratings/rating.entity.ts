import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { IRating } from "@shared/shared/rating/rating.interface";
import { Player } from "../players/player.entity";

@Entity()
export class Rating implements IRating {
    @PrimaryGeneratedColumn()
    id: number;

    // See example of developer (pleerock) https://github.com/typeorm/typeorm/issues/447#issuecomment-298896602
    @Column({ type: "int", nullable: true })
    playerId: number;

    @ManyToOne(() => Player)
    player: Player;

    @Column()
    value: number;
}
