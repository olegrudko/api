export interface WinData {
    playerId: number;       // player that reach finish
    place: number;          // win place
    currentRating: number;  // current player rating
    averageRating: number;  // average rating value between all players
}
