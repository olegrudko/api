export interface IPlayer {
    id: number;
    nickname: string;
}
