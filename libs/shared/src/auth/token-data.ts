export interface TokenData {
    username: string;
    userId: string | number;
    playerId: string | number;
    nickname: string;
}
