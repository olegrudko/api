export interface IRating {
    id: number;
    playerId: number;
    value: number;
}
