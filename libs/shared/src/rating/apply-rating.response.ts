export class ApplyRatingResponse {
    playerId: number;
    oldRating: number;
    newRating: number;
}
