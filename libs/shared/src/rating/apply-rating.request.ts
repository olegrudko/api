export class ApplyRatingRequest {
    playerId: number;
    averageRating: number;
    position: number;
}
