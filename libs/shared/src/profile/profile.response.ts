export class ProfileResponse {
    nickname: string;
    rating: number;
    playerId: number | string;
}
