export enum DBTableName {
    user = "user",
    player = "player",
    rating = "rating",
}