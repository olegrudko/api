export const config = {
    management: {
        url: process.env.MANAGEMENT_URL || "http://localhost:3001",
    },
};
