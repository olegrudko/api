import * as superagent from "superagent";
import { config } from "./config";
import { IUser } from "@unsav/shared";

interface LoginResponse {
    access_token: string;
}

export class AuthClientService {
    public async login(username: string, password: string): Promise<LoginResponse> {
        const response = await superagent
            .post(`${config.management.url}/auth/login`)
            .send({ username, password });

        return {
            access_token: response.body.access_token,
        };
    }

    public async registration(body: any): Promise<IUser> {
        const response = await superagent
            .post(`${config.management.url}/auth/registration`)
            .send(body);

        return response.body;
    }
}
