import * as superagent from "superagent";
import { config } from "./config";
import { ProfileResponse } from "@unsav/shared";

export class ProfileService {
    public async findOne(playerId: string): Promise<ProfileResponse> {
        const response = await superagent
            .get(`${config.management.url}/profile/${playerId}`)
            .send();

        return response.body;
    }
}
