import * as superagent from "superagent";
import { config } from "./config";
import { ApplyRatingResponse, ApplyRatingRequest } from "@unsav/shared";

export class RatingClientService {
    public async applyRating(applyRatingRequest: ApplyRatingRequest): Promise<ApplyRatingResponse> {
        const response = await superagent
            .post(`${config.management.url}/rating/apply`)
            .send(applyRatingRequest);
        return response.body;
    }

    public async getRating(playerId: number): Promise<ApplyRatingResponse> {
        const response = await superagent.get(`${config.management.url}/rating/${playerId}`).send();
        return response.body;
    }
}
