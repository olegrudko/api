export default {
    name: process.env.DB_NAME || "auth",
    type: process.env.DB_TYPE || "postgres",
    database: process.env.DB_NAME || "auth",
    port: +process.env.DB_PORT || 5432,
    username: process.env.DB_USERNAME || "postgres",
    password: process.env.DB_PASSWORD || "postgres",
    host: process.env.DB_HOST || "localhost",
    entities: ["apps/**/*.entity.ts", "libs/**/*.entity.ts"],
    migrations: ["migrations/*.ts"],
    cli: {
        migrationsDir: "migrations",
    },
};
